import Constants from './constants'
import state from './state'

export default {
    [Constants.FETCH_LIST]: (state, payload) => {
        state.hospitalList = payload
    },
    [Constants.FETCH_PAGE]: (state, payload) => {
        state.page = payload
    },
    [Constants.FETCH_TOTAL_COUNT]: (state, payload) => {
        state.totalCount = payload
    },
    [Constants.FETCH_MATCH_COUNT]: (state, payload) => {
        state.matchCount = payload
    },
}
