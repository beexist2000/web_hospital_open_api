import axios from 'axios'
import apiUrls from "./apiUrls"
import Constants from  "./constants"

export default {
    [Constants.DO_LIST]: (store, payload) => {
        const serviceKey = 'dEwAqrrgiUcCdt7d5yrhzBdqh%2FzoM7iN45D55Bm5z2XFaFDNyW06c%2FiMnKoJLCnlefaHJJf54nxeMKI3Wp0H3A%3D%3D'
        return axios.get(`${apiUrls.DO_LIST}?serviceKey=${serviceKey}&page=${payload.page}&cond%5BorgZipaddr%3A%3ALIKE%5D=${payload.searchKeyword}`)
            .then((res) => {
                store.commit(Constants.FETCH_LIST, res.data.data)
                store.commit(Constants.FETCH_PAGE, res.data.page)
                store.commit(Constants.FETCH_TOTAL_COUNT, res.data.totalCount)
                store.commit(Constants.FETCH_MATCH_COUNT, res.data.matchCount)
            })
    }
}
