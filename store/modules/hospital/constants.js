export default {
    DO_LIST: 'hospital/doList',
    GET_LIST: 'hospital/getList',
    FETCH_LIST: 'hospital/fetchList',
    GET_PAGE: 'hospital/getPage',
    FETCH_PAGE: 'hospital/fetchPage',
    GET_TOTAL_COUNT: 'hospital/getTotalCount',
    FETCH_TOTAL_COUNT: 'hospital/fetchTotalCount',
    GET_MATCH_COUNT: 'hospital/getMatchCount',
    FETCH_MATCH_COUNT: 'hospital/fetchMatchCount',
}
