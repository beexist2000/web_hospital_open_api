const state = () => ({
    hospitalList: [],
    page: 1,
    totalCount: 0,
    matchCount: 0,
})

export default state
